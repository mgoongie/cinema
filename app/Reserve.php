<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Reserve extends Model
{
    protected $fillable = ['film_id', 'seat_id'];

    public function seats()
    {
        return DB::table('seats')->LeftJoin('reserves', 'seats.id', '=', 'reserves.seat_id')->get();
    }
}
