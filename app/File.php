<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class File extends Model
{
    protected $fillable = ['film_id', 'name'];


    public function films()
    {
        return DB::table('files')
            ->select('files.*');

    }
}
