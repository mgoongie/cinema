<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Timetable extends Model
{

    protected $fillable = ['film_id', 'hall_id', 'start_time', 'date'];

    public function information()
    {
        return DB::table('films')
            ->select('films.name', 'description', 'duration', 'director', 'start_time', 'date', 'genre', 'hall_id', 'films.id')
            ->leftJoin('categories', 'films.category_id', '=', 'categories.id')
            ->leftJoin('timetables', 'timetables.film_id', '=', 'films.id')
            ->get();
    }
}
