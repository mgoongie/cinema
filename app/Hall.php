<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Hall extends Model
{
    protected $fillable = ['type'];


    public function timetable()
    {
        return DB::table('halls')->join('timetables', 'halls.id', '=', 'timetables.hall_id')->get();
    }
}
