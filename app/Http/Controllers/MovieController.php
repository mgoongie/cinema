<?php

namespace App\Http\Controllers;

use App\Film;
use App\File;
use App\Category;
use App\Hall;
use App\Timetable;
use function Couchbase\defaultDecoder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Symfony\Component\VarDumper\Dumper\DataDumperInterface;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::query()->get();
        $halls = Hall::query()->get();
        return view('movie', ([
            'categories' => $categories,
            'halls' => $halls
        ]));
    }

    public function singlePage(Request $request)
    {
//        dd($request->all());
        $movie = new Film;
        $movie = $movie->info()->where('films.id', $request->movie_id)->first();
        $files = new File();
        $files = $files->films()->where('film_id', $request->movie_id)->get();
        return view('movie-card', [
            'movie' => $movie,
            'files' => $files
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        checkbox values
        $request->merge([
            'categories' => implode(',', (array)$request->get('categories'))
        ]);
        $movie = Film::query()->create([
            'name' => $request->name,
            'description' => $request->description,
            'duration' => $request->duration,
            'category_id' => $request->categories,
            'director' => $request->director
        ]);
//storing timetable data
        $this->storeInTimetable($movie->id, $request->hall, $request->date, $request->time);
//        file uploading
        $f_id = $this->uploadFiles($request->file('files'), $movie->id);
        $this->film_file($movie->id, $f_id);
        return redirect()->back();
    }

    public function uploadFiles($files, $id)
    {
        $ids = [];
        foreach ($files as $file) {
            $file->move(public_path('image'), $file->getClientOriginalName());
            $movies[] = File::query()->create([
                'name' => 'image/' . $file->getClientOriginalName(),
                'film_id' => $id
            ]);
        }
        foreach ($movies as $file) {
            $ids[] = $file->id;
        }
        return $ids;
    }

    public function storeInTimetable($movieId, $hallId, $date, $time)
    {
        Timetable::query()->create([
            'film_id' => $movieId,
            'hall_id' => $hallId,
            'start_time' => $time,
            'date' => $date,
        ]);
    }

    public function film_file($movie_id, $file_ids)
    {
        foreach ($file_ids as $file_id) {
            DB::table('film_file')->insert([
                'file_id' => $file_id,
                'film_id' => $movie_id
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
