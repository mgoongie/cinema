<?php

namespace App\Http\Controllers;

use App\Hall;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $halls = Hall::query()->select('id', 'bg_image', 'type', 'color')->get();
        return view('home', compact('halls'));
    }
}
