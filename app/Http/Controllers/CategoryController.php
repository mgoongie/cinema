<?php

namespace App\Http\Controllers;

use App\Category;
use App\File;
use App\Film;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $category = Film::query()->get();
        dd($category);
        return view('category');
    }

    public function store(Request $request)
    {
        $category = Category::query()->create([
            'genre' => $request->name
        ]);
        return redirect()->back();
    }

}
