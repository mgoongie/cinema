<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;


class Category extends Model
{
    use SoftDeletes;
    protected $fillable = ['genre'];

    public function films()
    {
        return DB::table('categories')->
            LeftJoin('films', 'categories.id', '=', 'films.category_id')
            ->LeftJoin('files', 'films.id', '=', 'files.film_id')
            ->select(['files.name'])->get();
    }

}
