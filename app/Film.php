<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Film extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'description', 'duration', 'director', 'category_id'];

    public function info()
    {
        return DB::table('films')
            ->select('films.id','films.name as film_name', 'description', 'duration', 'category_id', 'director', 'file_id as f_id', 'genre', 'timetables.hall_id as h_id')
            ->LeftJoin('categories', 'films.category_id', '=', 'categories.id')
            ->join('timetables', 'films.id', '=', 'timetables.film_id');

    }

    public function files()
    {
        return DB::table('films')
            ->join('film_file', 'films.id', '=', 'film_file.film_id')
            ->where('film_file.Film_id', '=', 'films.id')
            ->get();
    }
}
