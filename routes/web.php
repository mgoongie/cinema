<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('reserves');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/hall', 'HallController@hall')->name('hall');
Route::resource('/movies', 'MovieController');
Route::get('/movie', 'MovieController@singlePage')->name('movie.show');
Route::get('/test', 'MovieController@index');
Route::get('/categories', 'CategoryController@index')->name('page');
Route::post('/categories', 'CategoryController@store')->name('category.store');
Route::resource('/admin', 'AdminController');
Route::resource('/reserves', 'ReserveController');


