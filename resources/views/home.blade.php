@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="main">
                <h1 class="cinema">Cinema Mgoongie</h1>
                <h2 class="halls">Halls</h2>
                <div class="row">
                    @foreach($halls as $hall)
                    <a class="item col-md-4" href="{{route('hall', ['hall' => 'red', 'id' => $hall->id])}}">
                        <p class="item_description">{{$hall->type}}</p>
                        <div class="image"
                             style="background-image: url({{$hall->bg_image}}); border-radius: 50%; background-color: {{$hall->color}}"></div>
                    </a>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
@endsection
