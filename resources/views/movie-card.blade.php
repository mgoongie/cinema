@extends('layouts.app')
@section('content')
    {{--    @php dd($movie) @endphp--}}
    <div class="container">
        <div class="row">
            <div class="movie_card" id="ave">
                <div class="info_section">
                    <div class="movie_header">
                        <img class="locandina" src="{{$files->first()->name}}"/>
                        <h1 class="header">{{$movie->film_name}}</h1>

                    </div>
                    <div class="movie_desc">
                        <h4 class="header">{{$movie->director}}</h4>
                        <p class="text"> {{$movie->description}}</p>
                        <span class="minutes">{{$movie->duration}}</span>
                        <p class="type"> {{$movie->genre}}</p>
                    </div>
                    <div class="movie_social">
                        <ul>
                            <li><i class="fa fa-share"></i></li>
                            <li><i class="fa fa-heart"></i></li>
                            <li><i class="fa fa-comment"></i></li>
                            <li><a class="btn btn-success" href="{{route('reserves.index', [ 'movie' => $movie->id])}}">
                                    {{ __('Reserve a
                                        seat') }}
                                </a></li>
                        </ul>
                    </div>
                </div>
                <div class="blur_back ave_back" style="background: url('{{$files->last()->name}}')"></div>
            </div>
        </div>
    </div>
@endsection
