@extends('layouts.app')
@section('content')
    <form method="POST" action="{{ route('category.store') }}" class="col-md-5 mx-auto mt-5" id="product_form" enctype="multipart/form-data">
        @csrf
        <div class="form-group row">
            <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('genre') }}</label>
            <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="name" autofocus>
                <span role="alert">
                        <strong class="errors"></strong>
                    </span>
            </div>
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-3">
                <button type="submit" class="btn btn-primary">
                    {{ __('Save') }}
                </button>
            </div>
        </div>
    </form>
@endsection