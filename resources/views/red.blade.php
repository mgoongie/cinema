@extends('layouts.app')
@section('content')
    @foreach($movies as $movie)
    <div class="movie_card" id="ave">
        <div class="info_section">
            <div class="movie_header">
                <img class="locandina" src="{{$movie->name}}"/>
                <h1>{{$movie->f_name}}</h1>
                <h4>{{$movie->director}}</h4>
                <span class="minutes">{{$movie->duration}}</span>
                <p class="type">{{$movie->genre}}</p>
            </div>
            <div class="movie_desc">
                <p class="text">
                    {{$movie->description}}
                </p>
            </div>
            <div class="movie_social">
                <ul>
                    <li><i class="fa fa-share"></i></li>
                    <li><i class="fa fa-heart"></i></li>
                    <li><i class="fa fa-comment"></i></li>
                </ul>
            </div>
        </div>
        <div class="blur_back ave_back"></div>
    </div>
    @endforeach
@endsection
