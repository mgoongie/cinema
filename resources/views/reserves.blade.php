@extends('layouts.app')
@section('content')
    <form method="post" action="{{route('reserves.store')}}">
        @csrf
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h1>Reserve your seat</h1>
                </div>
                <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid w-25">
                    @for($i=1; $i <=81; $i++)
                        <label><input class="uk-checkbox" type="checkbox" id="{{$i}}" name="seat[]"
                                      value="{{$i}}"></label>
                    @endfor
                    @foreach($movies as $movie)
                        <input type="hidden" value="{{$movie->id}}" name="id">
                    @endforeach
                    <button type="submit" class="btn btn-success"> Save</button>
                </div>
            </div>
        </div>
    </form>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
@endsection