@extends('layouts.app')
@section('content')
    <form method="POST" action="{{ route('movies.store') }}" class="col-md-5 mx-auto mt-5" id="product_form"
          enctype="multipart/form-data">
        @csrf
        <div class="form-group row">
            <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('Name') }}</label>
            <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="name" autofocus>
                <span role="alert">
                        <strong class="errors"></strong>
                    </span>
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-md-3 col-form-label text-md-right">{{ __('Description') }}</label>
            <div class="col-md-6">
                <input id="description" type="text" class="form-control" name="description">
                <span role="alert">
                        <strong class="errors"></strong>
                    </span>
            </div>
        </div>
        <div class="form-group row">
            <label for="time" class="col-md-3 col-form-label text-md-right">{{ __('Duration (minutes)') }}</label>
            <div class="col-md-6">
                <input id="time" type="number" class="form-control"
                       name="duration">
                <span role="alert">
                        <strong class="errors"></strong>
                    </span>
            </div>
        </div>
        <div class="form-group row">
            <label for="category" class="col-md-3 col-form-label text-md-right">{{ __('Genre') }}</label>
            <div class="col-md-6">
                <div class="col-md-4">
                    @foreach($categories as $category)
                        <div class="round">
                            <input type="checkbox" id="cat{{$category->id}}" name="categories[]"
                                   value="{{$category->id}}">
                            <label for="cat{{$category->id}}">
                                {{ $category->genre }}
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="start" class="col-md-3 col-form-label text-md-right">{{ __('Start time') }}</label>
            <div class="col-md-6">
                <div class="input-group clockpicker" data-placement="right" data-align="top" data-autoclose="true">
                    <input type="text" class="form-control" name="time" id="start">
                    <span class="input-group-addon">
		        <span class="glyphicon glyphicon-time"></span>
	        </span>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="date" class="col-md-3 col-form-label text-md-right">{{ __('Date') }}</label>
            <div class="col-md-6">
                    <input type="date" class="form-control" name="date" id="date">
            </div>
        </div>
        <div class="form-group row">
            <label for="director" class="col-md-3 col-form-label text-md-right">{{ __('Director') }}</label>
            <div class="col-md-6">
                <input id="director" type="text" class="form-control"
                       name="director">
                <span role="alert">
                        <strong class="errors"></strong>
                    </span>
            </div>
        </div>
        <div class="form-group row">
            <label for="halls" class="col-md-3 col-form-label text-md-right">{{__('Hall')}}</label>
            <div class="col-md-6">
                <select name="hall" id="halls" class="form-control">
                    @foreach($halls as $hall)
                        <option value="{{$hall->id}}"> {{ $hall->type }}</option>
                    @endforeach
                </select>
                <span role="alert">
                        <strong class="errors"></strong>
                    </span>
            </div>
        </div>
        <div class="form-group row">
            <label for="upload" class="col-md-3 col-form-label text-md-right">{{ __('Upload') }}</label>
            <div class="col-md-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                    </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="inputGroupFile01"
                               aria-describedby="inputGroupFileAddon01" name="files[]" multiple>
                        <span></span>
                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-3">
                <button type="submit" class="btn btn-primary">
                    {{ __('Save') }}
                </button>
            </div>
        </div>
    </form>

@endsection