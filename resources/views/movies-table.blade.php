@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <table class="table">
                <thead class="bg-warning">
                <tr>
                    {{--<th scope="col">Id</th>--}}
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Duration</th>
                    <th scope="col">Director</th>
                    <th scope="col">Genres</th>
                    <th scope="col">Date</th>
                    <th scope="col">Start time</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($allMovies as $movie)
                    <tr>
                        {{--<td>{{$movie->id}}</td>--}}
                    <td><a href="{{route('movie.show', [ 'movie_id' => $movie->id ])}}">{{$movie->name}}</a></td>
                        <td>{{$movie->description}}</td>
                        <td>{{$movie->duration}}</td>
                        <td>{{$movie->director}}</td>
                        <td>{{$movie->genre}}</td>
                        <td>{{$movie->date}}</td>
                        <td>{{$movie->start_time}}</td>
                        <td>
                            <button class="btn btn-success">Edit</button>
                        </td>
                        <td>
                            <button class="btn btn-danger">Delete</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection